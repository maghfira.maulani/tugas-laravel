<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
    	return view('register');
    }

    public function register(Request $request) {
    	$first = $request['first'];
    	$last = $request['last'];
    	return view('welcome',compact('first','last'));
    }
}
