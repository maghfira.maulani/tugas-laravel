<!DOCTYPE html>
<html>
<head>
	<title>Sign Up Form</title>
</head>
<body>

<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/register" method="POST">
	@csrf
	<p>First name:</p>
	<input type="text" name="first">
	<p>Last name:</p>
	<input type="text" name="last">
	<p>Gender:</p>
		<input type="radio" id="genderm" name="gender" value="Male">
		<label for="genderm">Male</label><br>
		<input type="radio" id="genderf" name="gender" value="Female">
		<label for="genderf">Female</label><br>
		<input type="radio" id="gendero" name="gender" value="Other">
		<label for="gendero">Other</label>
	<p>Nationality</p>
	<select>
	    <option>Indonesian</option>
	    <option>Korean</option>
	    <option>Japanese</option>
	</select>
	<p>Language Spoken:</p>
		<input type="checkbox" name="Indonesian"> Bahasa Indonesia <br>
    	<input type="checkbox" name="English"> English <br>
    	<input type="checkbox" name="Other"> Other <br>
	<p>Bio:</p>
	<textarea name="bio" rows="5" cols="20"></textarea>
	<br><input type="submit" value="submit">
	
</form>
<!-- <p><a href="D:\SanbercodeLaravel\welcome.html"></a></p> -->
</body>
</html>